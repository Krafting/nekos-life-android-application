const sfw = [
    "waifu",
    "neko",
    "shinobu",
    "megumin",
    "bully",
    "cuddle",
    "cry",
    "hug",
    "awoo",
    "kiss",
    "lick",
    "pat",
    "smug",
    "bonk",
    "yeet",
    "blush",
    "smile",
    "wave",
    "highfive",
    "handhold",
    "nom",
    "bite",
    "glomp",
    "slap",
    "kill",
    "kick",
    "happy",
    "wink",
    "poke",
    "dance",
    "cringe"
]
const nsfw = [
    "neko",
    "waifu",
    "trap",
    "blowjob"
]

function getContent(url) {
    console.log(url);
    document.getElementById('image').src = "img/loading4.gif";
    fetch(url).then((response)=>{
        return response.json(); 
    }).then(data=>{
        // console.log(data.url)
        document.getElementById('image').src = data.url
        document.getElementById('linkPicture').href = data.url
    });
    var totalGenerate = localStorage.getItem('totalGenerate');
    if(totalGenerate == null) {
        localStorage.setItem('totalGenerate', 1)
    } else {
        localStorage.setItem('totalGenerate', parseInt(totalGenerate)+1)
    }
    var imageElement = document.getElementById('image')
    imageElement.style.transform = "";
    imageElement.style.WebkitTransform = "";
    imageElement.style.zIndex = "";
}

function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    var blob = new Blob([ab], {type: mimeString});
    return blob;
}

function DownloadToDevice() {
    document.getElementById('settingIcon').classList.add('spinning');
    document.getElementById('settingIcon').classList.replace('fa-plus-circle', 'fa-spinner');
    document.getElementById('navbarScrollingDropdown').style.backgroundColor = "var(--bs-body-bg-secondary)";
    var fileurl = document.getElementById('image').src;
    var xhr = new XMLHttpRequest();
    var url = "https://api.allorigins.win/get?url=" + encodeURIComponent(fileurl);
    var filename = fileurl.substring(fileurl.lastIndexOf('/')+1);
    xhr.open("GET", url, true);
    xhr.responseType = "json";
    xhr.onload = function() {
        console.log("test");
        blob = xhr.response;
        // if(blob.toString().startswith("Access to XMLHttpRequest at ")) {
        //     console.log("test");
        // }
        blob = blob.contents
        console.log(blob);
        console.log("Saving...");
        var storageLocation = "";
        switch (device.platform) {
            case "Android":
                storageLocation = 'file:///storage/emulated/0/';
                break;
            case "iOS":
                storageLocation = cordova.file.documentsDirectory;
                break;
            case "Linux":
                storageLocation = "~/Downloads";
                break;
        }
        var folderpath = storageLocation + "Download";
        const base64Response = dataURItoBlob(blob);
        window.resolveLocalFileSystemURL(folderpath, function(dir) {
            dir.getDirectory("NekosLife", { create: true }, function (directory) {
                console.log('Creating folder..');
           });
        });
        folderpath = folderpath + "/NekosLife";
        console.log(folderpath)
        window.resolveLocalFileSystemURL(folderpath, function(dir) {
            dir.getFile(filename, {create:true}, function(file) {
                file.createWriter(function(fileWriter) {
                    fileWriter.write(base64Response);
                    document.getElementById('settingIcon').classList.remove('spinning');
                    document.getElementById('settingIcon').classList.replace('fa-spinner', 'fa-check');
                    document.getElementById('navbarScrollingDropdown').style.backgroundColor = "green";

                    document.getElementById('savedPicSuccess').style.display = "block";

                    setTimeout(function() {
                        document.getElementById('navbarScrollingDropdown').style.backgroundColor = "var(--bs-body-bg-secondary)";
                        document.getElementById('settingIcon').classList.replace('fa-check', 'fa-plus-circle');
                        document.getElementById('savedPicSuccess').style.display = "none";
                    }, 4000);
                    //Download was succesfull
                }, function(err){
                    // failed
                    console.log(err);
                });
            });
        });
    }
    xhr.onerror = function() {
        document.getElementById('settingIcon').classList.remove('spinning');
        document.getElementById('settingIcon').classList.replace('fa-spinner', 'fa-times');
        document.getElementById('navbarScrollingDropdown').style.backgroundColor = "red";

        document.getElementById('savedError').style.display = "block";

        setTimeout(function() {
            document.getElementById('navbarScrollingDropdown').style.backgroundColor = "var(--bs-body-bg-secondary)";
            document.getElementById('settingIcon').classList.replace('fa-times', 'fa-plus-circle');
            document.getElementById('savedError').style.display = "none";
        }, 4000);
    }
    xhr.send();
}


function getRandNeko() {
    getContent("https://api.waifu.pics/sfw/neko");
}

function getRandNekoLewd() {
    getContent("https://api.waifu.pics/nsfw/neko");
}
function getSFWTag(tag) {
    getContent("https://api.waifu.pics/sfw/"+tag);
}

function getNSFWTag(tag) {
    getContent("https://api.waifu.pics/nsfw/"+tag);
}

function getTag(tag, type = "sfw") {
    if (type === "nsfw") {
        getNSFWTag(tag);
    } else {
        getSFWTag(tag);
    }
}

var btnTag = document.getElementById('selectTag');
btnTag.onclick = function () {
    let modal = btnTag.getAttribute("data-modal");
    document.getElementById(modal).style.display = "block";
};
var btnTagClose = document.getElementById('selectTagClose');
btnTagClose.onclick = function () {
    let modal = btnTagClose.closest(".modal");
    modal.style.display = "none";
};

const pinchZoom = (imageElement) => {
    let imageElementScale = 1;
  
    let start = {};
  
    // Calculate distance between two fingers
    const distance = (event) => {
      return Math.hypot(event.touches[0].pageX - event.touches[1].pageX, event.touches[0].pageY - event.touches[1].pageY);
    };
  
    imageElement.addEventListener('touchstart', (event) => {
      // console.log('touchstart', event);
      if (event.touches.length === 2) {
        event.preventDefault(); // Prevent page scroll
  
        // Calculate where the fingers have started on the X and Y axis
        start.x = (event.touches[0].pageX + event.touches[1].pageX) / 2;
        start.y = (event.touches[0].pageY + event.touches[1].pageY) / 2;
        start.distance = distance(event);
      }
    });
  
    imageElement.addEventListener('touchmove', (event) => {
      // console.log('touchmove', event);
      if (event.touches.length === 2) {
        event.preventDefault(); // Prevent page scroll
  
        // Safari provides event.scale as two fingers move on the screen
        // For other browsers just calculate the scale manually
        let scale;
        if (event.scale) {
          scale = event.scale;
        } else {
          const deltaDistance = distance(event);
          scale = deltaDistance / start.distance;
        }
        imageElementScale = Math.min(Math.max(1, scale), 4);
  
        // Calculate how much the fingers have moved on the X and Y axis
        const deltaX = (((event.touches[0].pageX + event.touches[1].pageX) / 2) - start.x) * 2; // x2 for accelarated movement
        const deltaY = (((event.touches[0].pageY + event.touches[1].pageY) / 2) - start.y) * 2; // x2 for accelarated movement
  
        // Transform the image to make it grow and move with fingers
        const transform = `translate3d(${deltaX}px, ${deltaY}px, 0) scale(${imageElementScale})`;
        imageElement.style.transform = transform;
        imageElement.style.WebkitTransform = transform;
        imageElement.style.zIndex = "9999";
      }
    });
  
    imageElement.addEventListener('touchend', (event) => {
      // console.log('touchend', event);
      // Reset image to it's original format
      imageElement.style.transform = "";
      imageElement.style.WebkitTransform = "";
      imageElement.style.zIndex = "";
    });
}

window.onclick = function (event) {
    if (event.target.className === "modal") {
        event.target.style.display = "none";
    }
};

// Quand on clique sur le send Tag
document.getElementById('sendTagButton').onclick = function() {
    var dropdown = document.getElementById('dropdownTag').value;
    var input = document.getElementById('inputTag').value;
    var typeCheckedNSFW = document.getElementById('FormTagTypeNSFW').checked;
    if(typeCheckedNSFW) {
        type = "nsfw";
    } else {
        type = "sfw";
    }
       
    if(dropdown == "Select something.." && input == "") {
        console.log("Erreur: deux input vide");
        document.getElementById('alertNoTags').style.display = "block";
        document.getElementById('alertTagNotExist').style.display = "none";
        return
    }
    if(input != "") {
        var valueTag = input
    } else {
        var valueTag = dropdown
    }

    if( (nsfw.includes(valueTag) && type === "nsfw") ||  (sfw.includes(valueTag) && type === "sfw") ) {
        document.getElementById('alertNoTags').style.display = "none";
        document.getElementById('alertTagNotExist').style.display = "none";
        location.replace('tags.html?tag=' + valueTag + "&type=" + type);
    } else {
        document.getElementById('alertNoTags').style.display = "none";
        document.getElementById('alertTagNotExist').style.display = "block";
    }
};

// allTagsPossible
sfw.forEach(function(item) {
    var option = '<option value="'+item+'">'+ item +'</option>';
    document.getElementById('dropdownTag').insertAdjacentHTML('beforeend', option);
    document.getElementById('allTagsPossible').insertAdjacentHTML('beforeend', item+', ');
});

document.getElementById("FormTagTypeNSFW").addEventListener( 'click', (event) => {
    document.getElementById('dropdownTag').innerHTML = "<option selected>Select something..</option>"; 
    document.getElementById('allTagsPossible').innerHTML = ""; 
    nsfw.forEach(function(item) {
        var option = '<option value="'+item+'">'+ item +'</option>';
        document.getElementById('dropdownTag').insertAdjacentHTML('beforeend', option);
        document.getElementById('allTagsPossible').insertAdjacentHTML('beforeend', item+', ');
    });
});

document.getElementById("FormTagTypeSFW").addEventListener( 'click', (event) => {
    document.getElementById('dropdownTag').innerHTML = "<option selected>Select something..</option>"; 
    document.getElementById('allTagsPossible').innerHTML = ""; 
    sfw.forEach(function(item) {
        var option = '<option value="'+item+'">'+ item +'</option>';
        document.getElementById('dropdownTag').insertAdjacentHTML('beforeend', option);
        document.getElementById('allTagsPossible').insertAdjacentHTML('beforeend', item+', ');
    });
});



function getUrlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null) {
        return null;
    } else { 
        return results[1];
    }
};

function loadBar() {
    var setting = localStorage.getItem('settingBottomBar');
    if(setting == 1) {
        document.body.classList.add('bottomBar');
    } else {
        document.body.classList.remove('bottomBar');
    }
}
loadBar();

function loadRefresh() {
    var setting = localStorage.getItem('settingRefresh');
    var settingTime = localStorage.getItem('settingRefreshTime');
    var elementToClick = document.getElementById('requestNewPictureButton');
    if(elementToClick) {
        if(setting == 1 && settingTime) {
            setInterval(function() {
                elementToClick.click();
            }, settingTime);
        }
    }
}
loadRefresh()

// Quand on clique sur les notifications de download.
document.getElementById('savedPicSuccess').onclick = function() {
    this.style.display = "none"
}
// Quand on clique sur les erreurs
document.getElementById('savedError').onclick = function() {
    this.style.display = "none"
}
