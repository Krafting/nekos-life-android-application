# From newer to older

+ Added option to auto refresh
+ Added link to Gitlab
+ Added in-app notification and checks when a download fail
+ New API
+ Separate NSFW and SFW tags
+ Add CHANGELOG.md