# Nekos.Life Android Application

A fun application that I coded after seeing this github issue: https://github.com/Nekos-life/neko-apk/issues/1

No application? No problem, I made one!

# Features

* Nekos
* Lewd Nekos
* Select all Tags from the API
* Stats: number of generated images
* Auto-refresh
* A Quick Access Tag system
* Save Picture to Device

# Compile from source

Create a cordova project:

`cordova create Workspace/Project ext.domain.name AppName`

Go in the project directory and add the platform:

`cd Workspace/Project/`

`cordova platform add android`

Add the required plugin: 

`cordova plugin add cordova-plugin-file`

`cordova plugin add cordova-plugin-device`

Clone this repo & move everything into the project folder root then delete the created folder:

`git clone https://gitlab.com/Krafting/nekos-life-android-application`

`mv nekos-life-android-application/* ./ && mv nekos-life-android-application/.* ./`

`rm nekos-life-android-application/`

Run the application:

`cordova run android && adb shell logcat -c`

Build the application:

`cordova build android && mv ./platforms/android/app/build/outputs/apk/debug/app-debug.apk ./release/build-android-latest.apk`

Application should now be in the `release` folder

# Download the latest release

The latest release is in the `release` folder in this repo, or download it here directly:

[Click here to download](https://gitlab.com/Krafting/nekos-life-android-application/-/raw/main/release/nekoslife-android-latest.apk?inline=false)

# Stuff used

* Bootstrap
* Fork Awesome
* Waifu.pics Web API